import static java.util.stream.LongStream.rangeClosed;

/**
 * Created by salman on 25/02/2017.
 */
public class FizzBuzz {

    public static void main(String... arguments) {
        rangeClosed(1, 100)
                .mapToObj(i -> {
                    if (i % (3 * 5) == 0) {
                        return "FizzBuzz";
                    } else if (i % 3 == 0) {
                        return "Fizz";
                    } else if (i % 5 == 0) {
                        return "Buzz";
                    } else {
                        return Long.toString(i);
                    }
                })
                .forEach(System.out::println)
        ;
    }
}
